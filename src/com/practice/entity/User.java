package com.practice.entity;


/**
 * 
 * 
 * @author xyong
 * @date 2016-09-27 03:49:57
 */
public class User {


    /**
     * 主键
     */
    private String id;


    /**
     * 姓名
     */
    private String name;


    /**
     * 年龄
     */
    private Integer age;



    /**
     * 主键
     */
    public String getId() {
        return id;
    }

    /**
     * 主键
     */
    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    /**
     * 姓名
     */
    public String getName() {
        return name;
    }

    /**
     * 姓名
     */
    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    /**
     * 年龄
     */
    public Integer getAge() {
        return age;
    }

    /**
     * 年龄
     */
    public void setAge(Integer age) {
        this.age = age;
    }
}
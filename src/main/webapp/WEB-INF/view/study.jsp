<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="zh-cn">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Bootstrap 101 Template</title>

    <!-- Bootstrap -->
    <link href="${pageContext.request.contextPath}/static/js/bootstrap-3.3.0-dist/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="http://cdn.bootcss.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="http://cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
  	
	<h1 style="text-align: center;">小勇</h1>
	
	<div style="text-align: center;" id="content" name="content">
		<table style="margin:auto;" border="1px" width="800px">
			<tr>
				<td>你：</td>
				<td>吃饭了吗？</td>
			</tr>
			<tr>
				<td>小勇：</td>
				<td>吃饭了！</td>
			</tr>
		</table>
	</div>
	<br /><br /><br /><br /><br />
	<div style="text-align: center;">
		<sapn>提问：</sapn>
		<input type="text" id="question" name="question" />
		<input type="button" id="commit" name="commit" value="提问"/>
	</div>
	
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="${pageContext.request.contextPath}/static/js/jquery-3.1.1.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="${pageContext.request.contextPath}/static/js/bootstrap-3.3.0-dist/dist/js/bootstrap.min.js"></script>
  </body>
</html>



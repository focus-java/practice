package com.practice.servlet;

import java.io.File;
import java.io.IOException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.PropertyConfigurator;

public class Log4jInitServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	public Log4jInitServlet() {
		super();
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		super.doGet(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		//���󽻸�doGet
		doGet(req, resp);
	}

	@Override
	public void init(ServletConfig config) throws ServletException {
		System.out.println("Log4jInitServlet ��ʼ��־������Ϣ");
		String log4jLocation = config.getInitParameter("log4j-properties");
		ServletContext servletContext = config.getServletContext();

		if (log4jLocation == null) {
			System.err.println("*** û�ҵ� log4j-properties �����ļ������� BasicConfigurator ��������");
			BasicConfigurator.configure();
		} else {
			String webAppPath = servletContext.getRealPath("/");
			String log4jProp = webAppPath + log4jLocation;
			File yoMamaYesThisSaysYoMama = new File(log4jProp);
			if (yoMamaYesThisSaysYoMama.exists()) {
				System.out.println("·����" + log4jProp + "���ڣ�ʹ�� PropertyConfigurator ���д���");
				PropertyConfigurator.configure(log4jProp);
			} else {
				System.err.println("*** " + log4jProp + " û�ҵ� log4j-properties �����ļ������� BasicConfigurator �������� ");
				BasicConfigurator.configure();
			}
		}
		super.init(config);
	}

}

package com.practice.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.practice.dao.UserMapper;
import com.practice.entity.User;
import com.practice.entityCondition.UserCondition;

@Service
public class UserService {
	
	@Autowired
	private UserMapper userMapper;
	
	public List<User> findList(UserCondition condition){
		return userMapper.findList(condition);
	}
	
	
}

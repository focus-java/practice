package com.practice.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.practice.entity.User;
import com.practice.entityCondition.UserCondition;
import com.practice.service.UserService;

@Controller
@RequestMapping("/helloWorld")
public class HelloWorld {
	
	private static Logger logger = Logger.getLogger(HelloWorld.class);
	
	@Autowired
	private UserService userService;
	
	@RequestMapping("/index")
	public ModelAndView index(){
		ModelAndView mv = new ModelAndView("index");
		//这是 debug 级别的信息
        logger.debug("This is debug message.");
        //这是 info 级别的信息
        logger.info("This is info message.");
        //这是 error 级别的信息
        logger.error("This is error message.");
        
        List<User> uList = userService.findList(new UserCondition());
        mv.addObject("uList", uList);
		return mv;
	}
}

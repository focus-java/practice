package com.practice.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/study")
public class StudyController {
	
	@RequestMapping("/study")
	public ModelAndView study(){
		return new ModelAndView("study");
	}
	
}

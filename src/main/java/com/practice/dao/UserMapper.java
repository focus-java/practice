package com.practice.dao;

import com.practice.entity.User;
import java.util.List;


/**
 * Mapper类
 * 
 * @author xyong
 * @date 2016-09-27 05:20:53
 */
public interface UserMapper {

    /**
     * 新增实体类到数据库
     * 
     */
    int add(User record);

    /**
     * 根据主键删除该记录
     * 
     */
    int delete(String id);

    /**
     * 根据主键修改该记录
     * 
     */
    int update(User record);

    /**
     * 根据主键查询该记录
     * 
     */
    User findById(String id);

    /**
     * 根据查询条件进行模糊查询
     * 
     */
    List<User> findLike(User condition);

    /**
     * 根据查询条件进行匹配查询
     * 
     */
    List<User> findList(User condition);
}